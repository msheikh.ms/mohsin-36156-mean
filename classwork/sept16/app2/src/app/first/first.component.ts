import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-first',
  templateUrl: './first.component.html',
  styleUrls: ['./first.component.css']
})
export class FirstComponent implements OnInit {

  firstname = 'ms'

  person={
    name: 'steve',
    age : 52
  }

  color = 'red'

  category = 3



  constructor() { }

  ngOnInit(): void {
  }


  showalert(){
    alert('you clicked a button')
  }

}
