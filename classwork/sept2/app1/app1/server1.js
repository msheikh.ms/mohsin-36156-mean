// step 1: require express
const express = require('express')

// step 2: create express application
const app = express()

// step 3: ROUTE (mapping of http GET, url and handler (function))
// GET /
app.get('/', (request, response) => {
  console.log('GET / received bhej dea maine')
  response.end('lo aa gaya mai')
})

// step 4: listen on port 3000
app.listen(3000, '0.0.0.0', () => {
  console.log('server started on port 3000')
})