import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {
  email = ''
  password = ''

  httpClient: HttpClient
  constructor(  httpClient: HttpClient) { 
    this.httpClient = httpClient
  }

  ngOnInit(): void {
  }
  onSignin() {
    console.log(`email = ${this.email}`)
    console.log(`password = ${this.password}`)
  
  const body = {
    email : this.email,
    password: this.password
  }

  const url = 'http://localhost:3000/user/signin'
  const request = this.httpClient.post(url,body)
  request.subscribe(response =>{
    console.log(response)
    if (response['status'] == 'success') {
      const data = response['data']
      const firstName = data['firstName']
      const lastName = data['lastName']
      alert(`welcome ${firstName} ${lastName}`)
    } else {
      alert('invalid email or password')
    }
  })
 }



}
