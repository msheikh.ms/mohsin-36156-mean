var mobile = /** @class */ (function () {
    function mobile() {
    }
    //getter----------
    mobile.prototype.getModel = function () {
        return this.model;
    };
    mobile.prototype.getCompany = function () {
        return this.company;
    };
    mobile.prototype.getPrice = function () {
        return this.price;
    };
    Object.defineProperty(mobile.prototype, "Model", {
        //setter--------
        set: function (model) {
            this.model = model;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(mobile.prototype, "Company", {
        set: function (company) {
            this.company = company;
        },
        enumerable: false,
        configurable: true
    });
    mobile.prototype.setPrice = function (price) {
        this.price = price;
    };
    return mobile;
}());
var m1 = new mobile();
// m1.setModel('iphone')
// m1.setCompany("apple")
// m1.setPrice(1202210)
m1.Model = "samsung";
m1.Company = "sam";
m1.setPrice(4522);
console.log(m1);
