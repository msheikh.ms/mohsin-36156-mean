class person { 
    protected name: string = ''
    protected address: string = ''
    protected age: number = 0
  
    public constructor(name: string, address: string, age: number) {
      this.name = name
      this.address = address
      this.age = age
    }

    public printInfo() {
        console.log('----- person info ------')
        console.log(`name = ${this.name}`)
        console.log(`address = ${this.address}`)
        console.log(`age = ${this.age}`)

    }
  }

  class emp extends person {
      dept : string= ''


      public constructor(name: string, address: string, age: number, dept: string) {
        // calling super class constructor
        super(name, address, age)
        
        this.dept = dept
     }

     public printInfo() {
        super.printInfo()
        console.log('----- player info ------')
        console.log(`dept = ${this.dept}`)
      }
        
}

const emp1 = new emp('e1','kdjcdj',25,'electronic')
console.log(emp1);
 emp1.printInfo()