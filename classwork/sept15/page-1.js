// class person{
//     name : string
//     age : number
//     email: string
// }
// const p1 = new person()
// p1.name= "ksckbdvib"
// p1.age=35
// p1.email="kanfinNLN@gmail.com"
// console.log(p1);
var mobile = /** @class */ (function () {
    function mobile() {
    }
    //getter----------
    mobile.prototype.getModel = function () {
        return this.model;
    };
    mobile.prototype.getCompany = function () {
        return this.company;
    };
    mobile.prototype.getPrice = function () {
        return this.price;
    };
    //setter--------
    mobile.prototype.setModel = function (model) {
        this.model = model;
    };
    mobile.prototype.setCompany = function (company) {
        this.company = company;
    };
    mobile.prototype.setPrice = function (price) {
        this.price = price;
    };
    return mobile;
}());
var m1 = new mobile();
m1.setModel('iphone');
m1.setCompany("apple");
m1.setPrice(1202210);
console.log(m1);
