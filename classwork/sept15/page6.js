var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var person = /** @class */ (function () {
    function person(name, address, age) {
        this.name = '';
        this.address = '';
        this.age = 0;
        this.name = name;
        this.address = address;
        this.age = age;
    }
    person.prototype.printInfo = function () {
        console.log('----- person info ------');
        console.log("name = " + this.name);
        console.log("address = " + this.address);
        console.log("age = " + this.age);
    };
    return person;
}());
var emp = /** @class */ (function (_super) {
    __extends(emp, _super);
    function emp(name, address, age, dept) {
        var _this = 
        // calling super class constructor
        _super.call(this, name, address, age) || this;
        _this.dept = '';
        _this.dept = dept;
        return _this;
    }
    emp.prototype.printInfo = function () {
        _super.prototype.printInfo.call(this);
        console.log('----- player info ------');
        console.log("dept = " + this.dept);
    };
    return emp;
}(person));
var emp1 = new emp('e1', 'kdjcdj', 25, 'electronic');
console.log(emp1);
emp1.printInfo();
