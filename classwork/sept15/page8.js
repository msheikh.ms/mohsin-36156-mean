var rect = /** @class */ (function () {
    function rect() {
    }
    rect.prototype.draw = function () {
        console.log("draw rectangle");
    };
    rect.prototype.erase = function () {
        console.log("ereasing the drawing");
    };
    return rect;
}());
function fun1(drawable) {
    drawable.draw();
    drawable.erase();
}
var drawable1 = new rect();
drawable1.draw();
