var person = /** @class */ (function () {
    //constructor --------------------------
    function person(name, age) {
        if (age === void 0) { age = 0; }
        this.name = name;
        this.age = age;
    }
    Object.defineProperty(person.prototype, "Name", {
        //getter-------------------
        get: function () { return this.name; },
        //setter-------------------------------
        set: function (name) {
            this.name = name;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(person.prototype, "Age", {
        get: function () { return this.age; },
        set: function (age) {
            this.age = age;
        },
        enumerable: false,
        configurable: true
    });
    //utility------------
    person.prototype.canVote = function () {
        if (this.age >= 18) {
            console.log(this.name + "is eligible to vote");
        }
        else {
            console.log(this.name + "is not elegible t vote");
        }
    };
    return person;
}());
var p1 = new person("klrahul", 28);
console.log("person is = " + p1.Name);
console.log("" + p1.Age);
p1.canVote();
