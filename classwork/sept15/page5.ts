class person { 
    protected name: string = ''
    protected address: string = ''
    protected age: number = 0
  
    public constructor(name: string, address: string, age: number) {
      this.name = name
      this.address = address
      this.age = age
    }
  }

  class emp extends person {
      dept : string= ''


      public constructor(name: string, address: string, age: number, dept: string) {
        // calling super class (Person) constructor
        super(name, address, age)
        this.dept = dept
  }
}

const emp1 = new emp('e1','kdjcdj',25,'electronic')
console.log(emp1);
 