const express=require('express');
const bodyParser=require('body-parser');
const jwt=require('jsonwebtoken');
const config=require('./config');

//get morgan

const morgan=require('morgan');

//swagger for api documentation

const swaggerJSDoc=require('swagger-jsdoc')
const swaggerUi=require('swagger-ui-express')


// routers
const adminRouter=require('./admin/routes/admin')
const brandRouter=require('./admin/routes/brand')
const categoryRouter=require('./admin/routes/category')
const productRouter=require('./admin/routes/product')
const orderRouter = require('./admin/routes/order')
const reviewRouter = require('./admin/routes/review')


const app=express();
 

//routes
app.use(bodyParser.json());
app.use(morgan('combined'));


//sawgger initializatio (swagger use for documentation)

const swaggerOptions={
    definition:{
        info:{
            title:'Amazon Server (Admin Panel)',
            version:'1.0.0',
            description:'This is an express server for amazon application'
        }
    },
    apis:['./admin/routes/*.js']
}

const swaggerSpec=swaggerJSDoc(swaggerOptions)

app.use('/api-docs' , swaggerUi.serve , swaggerUi.setup(swaggerSpec));


//middleware to get user id 

function getAdminId(request,response,next){

    if(request.url=='/admin/signup' || request.url=='/admin/signin'){
        next();
    }else{

            try{
                const token=request.headers['token']

                const data=jwt.verify(token,config.secret)

                //this AdminId is accessoble to all the routes
                
                request.adminId=data['id'];

                next();//go to the actual routes 

            }catch(ex){
                response.status(401)
                response.send({status: 'error', error: 'protected api'
               })
            }

    }
}

app.use(getAdminId)


//routes added

app.use('/admin',adminRouter)
app.use('/brand',brandRouter)
app.use('/category',categoryRouter)
app.use('/product',productRouter)
app.use('/order', orderRouter)
app.use('/review', reviewRouter)

//listen server
app.listen(3000,'0.0.0.0',()=>{
    console.log('server started successfully on port No.3000');
})