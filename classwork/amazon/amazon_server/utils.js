function createResult(error,dbResult){
 
    return error ?createError(error):createSuccess(dbResult)
}

function createError(error){
    const result={};
    
        result['status']='error'
        result['error']=error

    return result;
}

function createSuccess(dbResult){

       const result={};
        result['status']='success'
        result['data']=dbResult;

    return result;
}

module.exports={
    createResult:createResult ,
    createError :createError ,
    createSuccess :createSuccess
}