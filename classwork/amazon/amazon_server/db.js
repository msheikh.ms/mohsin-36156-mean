const mysql=require('mysql2');

const pool=mysql.createPool({
    host:'localhost',
    user:'project',
    password:'project',
    database:'mystore',
    waitForConnections: true,
    connectionLimit: 10,
    queueLimit: 0
})

module.exports=pool;

