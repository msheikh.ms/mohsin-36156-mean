const express=require('express');
const db=require('../../db')
const utils=require('../../utils')
const config=require('../../config')
const crypto=require('crypto-js');
const jwt=require('jsonwebtoken');



const router=express.Router();

//start Routing


//1.get
//--------------------------------------------------

/**
 * @swagger
 *
 * /admin/profile:
 *   get:
 *     description: for getting administrator profile
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: login
 */

 router.get('/profile',(request,response)=>{
            

            const statement=`select firstName, lastName, email, phone from admin where id = ${request.adminId}`


            db.query(statement,(error,admins)=>{

                const result={}; 

                if(error){
                    result['status']='error'
                    result['error']=error;
                    response.send(result);
                }else{
                    if(admins.length==0){
                        response.send({status: 'error', error: 'admin does not exist'})
                    }else{
                        const admin=admins[0];

                        response.send(utils.createResult(error, admin))
                    }
                    
                }
                
                
            })

 })

//---------------------------------------------------

//---------------------------------------------------
//2.post
//--------------------------------------------------

/**
 * @swagger
 *
 * /admin/signup:
 *   post:
 *     description: signup admin
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: firstName
 *         description: admin firstName.
 *         in: formData
 *         required: true
 *         type: string
 *       - name: lastName
 *         description: admin lastName.
 *         in: formData
 *         required: true
 *         type: string
 *       - name: phone
 *         description: admin phone.
 *         in: formData
 *         required: true
 *         type: string 
 *       - name: email
 *         description: admin firstName.
 *         in: formData
 *         required: true
 *         type: string
 *       - name: password
 *         description: User's password.
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: login
 */

router.post('/signup',(request,response)=>{
    
    const {firstName ,lastName , phone , email ,password }=request.body

    const encryptedPassword=crypto.SHA256(password);

    const statement=`insert into admin(firstName ,lastName , phone , email, password)
                    values(
                        '${firstName}',
                        '${lastName}',
                        '${phone}',
                        '${email}',
                        '${encryptedPassword}') `

            db.query(statement,(error,data)=>{

                response.send(utils.createResult(error,data));
            })
})

/**
 * @swagger
 *
 * /admin/signin:
 *   post:
 *     description: Login to the application
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: email
 *         description: email use for login
 *         in: formData
 *         required: true
 *         type: string
 *       - name: password
 *         description: admin's password.
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: login
 */

router.post('/signin',(request,response)=>{

    const { email,password }=request.body

    const encryptedPassword=crypto.SHA256(password);

    const statement=`select * from admin where email='${email}' and password='${encryptedPassword}' `

    db.query(statement,(error,admins)=>{

        const result={}; 

        if(error){
            result['status']='error'
            result['error']=error;
            response.send(result);
        }else{
            if(admins.length==0){
                response.send({status: 'error', error: 'user does not exist'})
            }else{
                const admin=admins[0];

                const token=jwt.sign({id:admin['id']},config.secret);

                response.send(utils.createResult(error,{
                    "firstName":admin['firstName'],
                    "lastName":admin['lastName'],
                    token:token
                }))
            }
            
        }
        
        
    })
})
//---------------------------------------------------

//---------------------------------------------------
//3.put
//--------------------------------------------------

/**
 * @swagger
 *
 * /admin/edit:
 *   put:
 *     description: signup admin
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: firstName
 *         description: admin firstName.
 *         in: formData
 *         required: true
 *         type: string
 *       - name: lastName
 *         description: admin lastName.
 *         in: formData
 *         required: true
 *         type: string
 *       - name: phone
 *         description: admin phone.
 *         in: formData
 *         required: true
 *         type: string 
 *       - name: email
 *         description: admin firstName.
 *         in: formData
 *         required: true
 *         type: string
 *       - name: password
 *         description: admin's password.
 *         in: formData
 *         required: true
 *         type: string
 *       - name: id
 *         description: admin's id.
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: login
 */


router.put('/edit',(request,response)=>{
    response.send('admin edit');
})

//---------------------------------------------------
//4.delete
//--------------------------------------------------

/**
 * @swagger
 *
 * /admin/remove:
 *   delete:
 *     description: for delete administrator profile
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: login
 */
router.delete('/remove',(request,response)=>{
    response.send('admin remove');
})

//---------------------------------------------------


module.exports=router;