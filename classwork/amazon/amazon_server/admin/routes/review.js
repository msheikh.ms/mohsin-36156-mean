const express=require('express');
const db=require('../../db')
const utils=require('../../utils')
const config=require('../../config')
const crypto=require('crypto-js');
const jwt=require('jsonwebtoken');


const router=express.Router();

//start Routing


//1.get
//--------------------------------------------------

 router.get('/:productId',(request,response)=>{
    
    const { productId }=request.params;

    const statement=`select * from productReviews where productId=${productId}`

    db.query(statement,(error,dbResult)=>{
         
        response.send(utils.createResult(error,dbResult))
    })

 })

//---------------------------------------------------

//---------------------------------------------------
//2.post
//--------------------------------------------------

// router.post('/',(request,response)=>{
//     response.send('');
// })


// router.post('/',(request,response)=>{
//     response.send('');
// })
//---------------------------------------------------

//---------------------------------------------------
//3.put
//--------------------------------------------------
// router.put('/',(request,response)=>{
//     response.send('');
// })

//---------------------------------------------------
//4.delete
//--------------------------------------------------
router.delete('/:id',(request,response)=>{
    const { id }=request.params;

    const statement=`delete from productReviews where id=${id}`

    db.query(statement,(error,dbResult)=>{
         
        response.send(utils.createResult(error,dbResult))
    })
})

//---------------------------------------------------


module.exports=router;