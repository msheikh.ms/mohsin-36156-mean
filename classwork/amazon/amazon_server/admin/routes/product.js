const express=require('express');
const db=require('../../db')
const utils=require('../../utils')
const config=require('../../config')
const crypto=require('crypto-js');
const jwt=require('jsonwebtoken');


// multer: used for uploading document
const multer = require('multer')
const upload = multer({ dest: 'images/' })


const router=express.Router();

//start Routing


//1.get
//--------------------------------------------------

 router.get('/',(request,response)=>{

     const statement=`SELECT p.id , p.title , p.description , c.id as productCategoryId , c.title as productCategoryTitle ,  p.price ,p.isActive , b.id as productBrandId , b.title as productBrandTitle , p.image , p.createdOn
                     from product p INNER JOIN category c
                     ON p.category=c.id 
                     INNER JOIN brand b
                     ON b.id=p.brand;`

     db.query(statement,(error,products)=>{

         if(error){

            response.send(utils.createerror(error));

        }else{

                    //empty product collection

                    const product=[];

                    for (let index = 0; index < products.length; index++) {
                    
                            const tmpProduct = products[index]

                    const productObject={

                                            "id":tmpProduct['id'],
                                            "title":tmpProduct['title'],
                                            "description":tmpProduct['description'],
                                            "price":tmpProduct['price'],
                                            "Acive":tmpProduct['isActive'],
                                            "category":{
                                                        "productCategoryId":tmpProduct['productCategoryId'],
                                                        "productCategoryTitle":tmpProduct['productCategoryTitle'],
                                                        },
                                            "Brand":{
                                                        "productBrandId":tmpProduct['productBrandId'],
                                                        "productBrandTitle":tmpProduct['productBrandTitle']
                                                    
                                                    }
                        }

                        product.push(productObject);
            }
            response.send(utils.createSuccess(product));
        }

        
     })
 })

//---------------------------------------------------

//---------------------------------------------------
//2.post
//--------------------------------------------------


router.post('/upload-image/:productId',upload.single('image') ,  (request,response)=>{

    const { productId } = request.params;

    const fileName=request.file.filename;

    const statement=`update product set image = '${fileName}' where id=${productId}`

    db.query(statement,(error,data)=>{
        response.send(utils.createResult(error,data));
    })
})

router.post('/create',(request,response)=>{
    
    const {title,description,category,price,brand}=request.body

    const statement=`insert into product(title,description,category,price,brand) values('${title}','${description}' , '${category}' , '${price}' ,'${brand}')`

    db.query(statement,(error,data)=>{
        response.send(utils.createResult(error,data));
    })
})



//---------------------------------------------------

//---------------------------------------------------
//3.put
//--------------------------------------------------
router.put('/:id',(request,response)=>{
    
    const {id}=request.params;

    const {title,description,category,price,brand}=request.body

    const statement=`update  product set
                    title='${title}',
                    description='${description}',
                    category='${category}',
                    price='${price}',
                    brand='${brand}'
                    where id=${id};`

    db.query(statement,(error,data)=>{
        response.send(utils.createResult(error,data));
    })
})

router.put('/update-state/:id/:isActive',(request,response)=>{
    
    const {id ,isActive}=request.params;

    const statement=`update  product set
                    isActive=${isActive}
                    where id=${id};`

    db.query(statement,(error,data)=>{
        response.send(utils.createResult(error,data));
    })
})

//---------------------------------------------------
//4.delete
//--------------------------------------------------
router.delete('/:id',(request,response)=>{

     const { id }=request.params;

     const statement=`delete from product where id=${id}`

     db.query(statement,(error,brand)=>{
         response.send(utils.createResult(error,brand));
     })
})

//---------------------------------------------------


module.exports=router;