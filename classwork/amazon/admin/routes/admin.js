const express = require('express')
const db = require('../../db')
const crypto = require('crypto-js')
const utils = require('../../utils')
const jwt = require('jsonwebtoken')
const config = require('../../config')

const { userInfo } = require('os')
const { request } = require('https')



const router = express.Router()

// post--------------------------------------
/**
 * @swagger
 *
 * /admin/signup:
 *   post:
 *     description: For signing up an administrator
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: firstName
 *         description: first name of admin user
 *         in: formData
 *         required: true
 *         type: string
 *       - name: lastName
 *         description: last name of admin user
 *         in: formData
 *         required: true
 *         type: string
 *       - name: email
 *         description: email of admin user used for authentication
 *         in: formData
 *         required: true
 *         type: string
 *       - name: password
 *         description: admin's password.
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: successful message
 */
router.post('/signup',(request,response)=>{
const {firstName,lastName,email,password} = request.body

const encryptedPassword = crypto.SHA256(password)
const statement = `insert into admin (firstName,lastName,email,password) 
values('${firstName}','${lastName}','${email}','${encryptedPassword}');`

db.query(statement,(error,data)=>{
response.send(utils.createResult(error,data))
})

})


/**
 * @swagger
 *
 * /admin/signin:
 *   post:
 *     description: For signing in an administrator
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: email
 *         description: email of admin user used for authentication
 *         in: formData
 *         required: true
 *         type: string
 *       - name: password
 *         description: admin's password.
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: successful message
 */
router.post('/signin',(request,response)=>{
    const { email,password }=request.body

    const encryptedPassword=crypto.SHA256(password);
    const statement=`select * from admin where email='${email}' and password='${encryptedPassword}' `

    db.query(statement,(error,admins)=>{
        const result={}; 
        if(error){
            result['status']='error'
            result['error']=error;
            response.send(result);
        }else{
            if(admins.length==0){
                response.send({status: 'error', error: 'user does not exist'})
            }else{
                const admin=admins[0];
                const token=jwt.sign({id:admin['id']},config.secret);
                response.send(utils.createResult(error,{
                    "firstName":admin['firstName'],
                    "lastName":admin['lastName'],
                    token:token
                }))
            }       
        }
    
    })
})

//GET---------------------------------------------------------

/**
 * @swagger
 *
 * /admin/profile:
 *   get:
 *     description: For getting administrator profile
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: successful message
 */
router.get('/profile',(request,response)=>{
            

   const statement=`select firstName, lastName, email, phone, address, city, zip, country from user where id = ${request.userId}`
      //  const statement =`select * from admin`    

    db.query(statement,(error,admins)=>{

        const result={}; 

        if(error){
            result['status']='error'
            result['error']=error;
            response.send(result);
        }else{
            if(admins.length==0){
                response.send({status: 'error', error: 'user does not exist'})
            }else{
                const admin=admins[0];

                response.send(utils.createResult(error, admin))
            }
            
        }
        
        
    })

})

  
module.exports = router