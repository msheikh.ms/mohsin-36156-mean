const express = require('express')
const utils = require('../../utils')
const db = require('../../db')
const { request, response } = require('express')
const router = express.Router()

//POST---------------------------------------------


router.post('/',(request,response)=>{
    const {title,description}= request.body
    const statement = `insert into brand (title,description) values ('${title}','${description}')`
    db.query(statement,(error,data)=>{
        response.send(utils.createResult(error,data))
    })
})

router.put('/:id',(request,response)=>{
    const {id}= request.params
    const {title,description}=request.body
    const statement = `update brand set title = '${title}', description = '${description}' where id = ${id}`
    db.query(statement,(error,data)=>{
        response.send(utils.createResult(error,data))
    })
})

//GET---------------------------------------------

router.get('/',(request,response)=>{
    const statement =`select * from brand`
    db.query(statement,(error,data)=>{
        response.send(utils.createResult(error,data))
    })
})




module.exports=router