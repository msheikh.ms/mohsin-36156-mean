import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cars',
  templateUrl: './cars.component.html',
  styleUrls: ['./cars.component.css']
})
export class CarsComponent implements OnInit {


cars = [ 
  {
    id:1,
    model:'i20',
    company:'hyundai',
    price:7.5,
    color:'white'
  },
  {
    id:2,
    model:'i10',
    company:'hyundai',   
    price:6.5,
    color:'black'
  }, {
    id:3,
    model:'nano',
    company:'tata',
    price:2.5,
    color:'yellow'
  }, {
    id:4,
    model:'fabia',
    company:'scoda',
    price:5.5,
    color:'gray'
  }
]




  constructor() { }

  ngOnInit(): void {
  }

}
