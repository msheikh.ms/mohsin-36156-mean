import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CarsComponent } from './cars/cars.component';
import { AccordionComponent } from './accordion/accordion.component';
import { RateComponent } from './rate/rate.component';
import { SmileyComponent } from './smiley/smiley.component';

@NgModule({
  declarations: [
    AppComponent,
    CarsComponent,
    AccordionComponent,
    RateComponent,
    SmileyComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
