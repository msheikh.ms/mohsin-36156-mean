const express = require('express')

const bodyParser = require('body-parser')
const mysql = require('mysql')
const { request, response } = require('express')
const app = express()

app.use(bodyParser.json())


app.get('/customer', (request, response) => {

    // creating a mysql connection
    const connection = mysql.createConnection({
      host: 'localhost',
      user: 'root',
      password: 'password',
      database: 'mydb'
    })
    connection.connect()
    const statement = `select id, name, password, mobile, address, email from customer`
    connection.query(statement, (error, data) => {

      connection.end()
  
      if (error) {
        response.end('error while executing query')
        console.log(`${error}`)
      } else {
        response.send(data)
      }
    })
  
  })
  app.post('/customer',(request,response)=>{
    const connection = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: 'password',
        database: 'mydb'
      })
      connection.connect()
    const statement = `insert into customer 
    (name, password, mobile, address, email) 
    values ('${request.body.name}', 
            '${request.body.password}',
            '${request.body.mobile}', 
            '${request.body.address}',
            '${request.body.email}')`
    connection.query(statement,(error,data)=>{
        console.log(`${error}`)
        connection.end()
        response.send(data)
    })
  })


  app.put('/customer',(request,response)=>{
      const connection = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: 'password',
        database: 'mydb'
      })
      connection.connect()
      const statement = `update customer 
      set name = '${request.body.name}',
      password = '${request.body.password}',
      mobile = '${request.body.mobile}',
      address = '${request.body.address}',
      email = '${request.body.email}'
      where id = ${request.body.id}`



      connection.query(statement,(error,data)=>{
        console.log(error)  
        connection.end()
        response.send(data)
      })
  })



  app.delete('/customer',(request,response)=>{
    const connection = mysql.createConnection({
      host: 'localhost',
      user: 'root',
      password: 'password',
      database: 'mydb'
    })
    connection.connect()
    const statement = `delete from customer where id = ${request.body.id}`

    connection.query(statement,(error,data)=>{
      console.log(error)  
      connection.end()
      response.send(data)
    })
})

app.listen(3000, '0.0.0.0', () => {
    console.log('server started on port 3000')
  })