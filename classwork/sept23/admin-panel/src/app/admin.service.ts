import { Routes, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AdminService {
  url = 'http://localhost:3000/admin'
  constructor( private httpClient:HttpClient,
                private router: Router) { }

  login(email:string,password:string){
    const body = {
      email:email,
      password:password
    }
    return this.httpClient.post(this.url + '/signin', body)
  }

  canActivate(route:ActivatedRouteSnapshot,state:RouterStateSnapshot){
    if (sessionStorage['token']){
      return true
    }
    else{
      this.router.navigate(['/login'])
      return false
    }
  }
}
