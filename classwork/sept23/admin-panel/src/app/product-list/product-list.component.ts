import { ProductService } from './../product.service';
import { Component, OnInit } from '@angular/core';
import { tick } from '@angular/core/testing';
import { Router } from '@angular/router';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  products =[]

  constructor(
    private router:Router,
    private productService:ProductService) { }

  ngOnInit(): void {
    this.loadProducts()
  }


  loadProducts(){
    this.productService
    .getProducts()
    .subscribe(response=>{
      if(response['status']=='success'){
        this.products=response['data']
      }else{
        console.log(response['error'])
      }
    })
  }

  toggleActiveStatus(product){
    this.productService
    .toggleActivateStatus(product)
    .subscribe(response=>{
      if(response['status']=='success'){
        this.loadProducts()

      }
      else{
        console.log(response['error']);
        
      }
    })
  }

  addProduct(){
    this.router.navigate(['/product-add'])

  }

  onEdit(product) {
    this.router.navigate(['/product-add'], {queryParams: {id: product['id']}})
  }

  onDelete(product){
    this.productService
    .deleteProduct(product)
    .subscribe(response=>{
      if(response['status']=='success'){
        this.loadProducts()
      }
      else{
        console.log(response['error']);
        
      }
    })
  }


  uploadImage(product){
    this.router.navigate(['/upload-image'],{queryParams:{id:product['id']}})
  }







}
