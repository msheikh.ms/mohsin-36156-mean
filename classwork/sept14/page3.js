var person = /** @class */ (function () {
    function person() {
    }
    person.prototype.printData = function () {
        console.log("name:" + this.firstName);
        console.log("Age : " + this.age);
    };
    return person;
}());
var p1 = new person();
console.log(p1);
p1.firstName = "mohsin";
p1.age = 25;
p1.printData();
