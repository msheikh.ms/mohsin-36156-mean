console.log('hello world');
let num =100
console.log(`num=${num}`);
let num1: number = 101
console.log(`num 1 = ${num1}`);

const person1: object = {
    name:'mohsin sheikh',
    age: 25,
    adress:'nagpur'
}
console.log(`person = ${person1}`);


const person: {name: string, age: number, address: string} 
    = {name: "person1", age: 40, address: "pune"}
console.log(`person = ${person}, type = ${typeof(person)}`)
