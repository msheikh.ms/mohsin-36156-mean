import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  url='http://localhost:3000/faculty'
  constructor(
    private httpClient:HttpClient
  ) { }


  Login(email:string,password : string){
 
    const body={
      email:email,
      password:password
    }
  
     return this.httpClient.post(this.url+"/signin",body)

  }



  
}
