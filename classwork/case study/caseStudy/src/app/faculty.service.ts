import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
@Injectable({
  providedIn: 'root'
})
export class FacultyService {
 url = 'http://localhost:3000/faculty'
  constructor(
    private httpClient:HttpClient
  ) { }


login(email:string , password:string){
  const body = {
    email:email,
    password:password
  }
  return this.httpClient.post(this.url,body)
}




}
