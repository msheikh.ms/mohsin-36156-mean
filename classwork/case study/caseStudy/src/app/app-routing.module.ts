import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { StudentComponent } from './student/student.component';
import { AdminComponent } from './admin/admin.component';
import { FacultyComponent } from './faculty/faculty.component';
import { HomeComponent } from './home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {path : 'home',component:HomeComponent},
  {path : 'faculty',component:FacultyComponent},
  {path : 'admin',component:AdminComponent},
  {path : 'student',component:StudentComponent},
  {path : 'login',component:LoginComponent},
  {path : 'signup',component:SignupComponent}





];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
