import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  url = 'http://localhost:3000/product'
  httpClient: HttpClient

  constructor( httpClient:HttpClient) { 
   this.httpClient = httpClient
  }
  getProducts() {
    console.log(this.url)
    return this.httpClient.get(this.url)
  }

   CreateProduct(title:string,description:string,price:number,category:number,brand:number){
     const body ={
      title :title,
      description:description,
      price:price,
      brand:brand,
      category:category
     }
     return this.httpClient.post(this.url,body)
   }

   deleteProduct(id: number) {
    // http://localhost:3000/product/1
    return this.httpClient.delete(this.url + "/" + id)
  }



}
