import { ProductService } from './../product.service';
import { Component, OnInit } from '@angular/core';
import { BrandService } from '../brand.service';
import { CategoryService } from '../category.service';

@Component({
  selector: 'app-product-add',
  templateUrl: './product-add.component.html',
  styleUrls: ['./product-add.component.css']
})
export class ProductAddComponent implements OnInit {


  title = ''
  description = ''
  category = 1
  brand = 1
  price = 0

  categories = []
  brands = []
  
  productService : ProductService
  brandService : BrandService
  categoryService : CategoryService


  constructor(productService : ProductService,
    brandService : BrandService,
    categoryService : CategoryService) { 
    this.productService = productService
    this.brandService = brandService
    this.categoryService  = categoryService
    }

  ngOnInit(): void {
    console.log('inside oninit()')
    this.loadData()
  }

  loadData(){
    this.categoryService
      .getCategories()
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.categories = response['data']
          console.log(this.categories)
        } else {
          console.log(response['error'])
        }
      })

    this.brandService
      .getBrands()
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.brands = response['data']
          console.log(this.brands)
        } else {
          console.log(response['error'])
        }
      })
  }

  onSave(){
    this.productService.CreateProduct(this.title, this.description, this.price, this.category, this.brand)
    .subscribe(response=>{
      if (response['status'] == 'success') {
        alert('product added successfully')
      } else {
        console.log(response['error'])
      }
    })
  }

}
