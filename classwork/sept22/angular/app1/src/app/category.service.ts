import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  url = 'http://localhost:3000/category'
   
  httpClient : HttpClient

  constructor( httpClient : HttpClient) { 
    this.httpClient=httpClient
  }
  getCategories(){
    return this.httpClient.get(this.url)
  }
}
