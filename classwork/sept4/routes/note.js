const express = require ('express')
const db = require('../db')
const utils = require('../utils')
const mysql = require('mysql2')
const { request, response } = require('express')

const router = express.Router()
//const connection = mysql.createConnection({
//        host: 'localhost',
//        user: 'root',
//        password: 'password',
//        database: 'notes_user'
//    })
router.get('/',(request,response)=>{
    const userid = request.body.userid
    const statement = ` select * from note where userid = '${userid}'`
   // connection.query(statement,(error,data)=>{
   //     if (error) {
   //        response.end('error while executing query')
   //        console.log(`${error}`)
   //      } else {
   //        response.send(data)
   //      }
   // })
  db.query(statement,(error,dbresult)=>{
      const result = utils.createResult(error,dbresult)
      response.send(result)
})

})

router.post('/',(request,response)=>{
  const userid = request.body.userid
  const content = request.body.content
  const statement = `insert into note (content,userid)
  values('${content}','${userid}');`
  db.query(statement,(error,dbresult)=>{
    const result = utils.createResult(error,dbresult)
    response.send(result)
    
  })
})
router.put('/',(request,response)=>{
  const id = request.body.id
  const userid = request.body.userid
  const content = request.body.content
  const statement = `update note set content ='${content}',userid='${userid}' where id = ${id}`
  db.query(statement,(error,dbresult)=>{
    const result = utils.createResult(error,dbresult)
    response.send(result)
  })
})


router.delete('/',(request,response)=>{
  const id = request.body.id
const statement = `delete from note where id = '${id}'`
db.query(statement,(error,dbresult)=>{
  const result = utils.createResult(error,dbresult)
  response.send(result)
})
})
module.exports = router