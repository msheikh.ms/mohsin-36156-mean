const express = require('express')
const router = express.Router()

const db = require('../db')
const utils = require('../utils')



// =======================================================================================================
// get
// =======================================================================================================
router.get('/', (request, response) => {
    const statement = `select * from books`
    db.query(statement, (error, data) => {

        response.send(utils.createResult(error, data))
    })


})
router.get('/:id', (request, response) => {
    const { id } = request.params
    const statement = `select * from books where id=${id}`
    db.query(statement, (error, data) => {

        response.send(utils.createResult(error, data))
    })


})
router.get('/searchBySubject/:subject', (request, response) => {
        const { subject } = request.params
        const statement = `select * from books where subject='${subject}'`


        db.query(statement, (error, data) => {

            response.send(utils.createResult(error, data))
        })


    })
    // =======================================================================================================
    // post
    // =======================================================================================================

router.post('/', (request, response) => {
    const { id, name, author, subject, price } = request.body
    const statement = `insert into books (id,name,author,subject,price) values('${id}','${name}','${author}','${subject}','${price}')`
    db.query(statement, (error, data) => {

        response.send(utils.createResult(error, data))
    })
})

// =======================================================================================================
// put
// =======================================================================================================


router.put('/:id', (request, response) => {
    const { id } = request.params
    const { name, author, subject, price } = request.body
    const statement = `update books set name='${name}',author='${author}',subject='${subject}',price='${price}' where id=${id}`
    db.query(statement, (error, data) => {

        response.send(utils.createResult(error, data))
    })
})

// =======================================================================================================
// delete
// =======================================================================================================


router.delete('/:id', (request, response) => {
    const { id } = request.params

    const statement = `delete from books where id=${id}`
    db.query(statement, (error, data) => {

        response.send(utils.createResult(error, data))
    })
})

module.exports = router