const express = require('express')
const router = express.Router()


const db = require('../db')
const utils = require('../utils')
const { request, response } = require('express')



// =======================================================================================================
// get
// =======================================================================================================

router.get('/', (request, response) => {
    const statement = `select * from customers`
    db.query(statement, (error, data) => {

        response.send(utils.createResult(error, data))
    })


})
router.get('/:id', (request, response) => {
    const { id } = request.params
    const statement = `select * from customers where id=${id}`
    db.query(statement, (error, data) => {

        response.send(utils.createResult(error, data))
    })


})



// =======================================================================================================
// post
// =======================================================================================================


router.post('/signin', (request, response) => {
    const { email, password } = request.body
    const statement = `select * from customers where email='${email}'and password='${password}'`
    db.query(statement, (error, users) => {

        if (error) {
            response.send(utils.createError(error))
        } else {
            if (users.length == 0) {
                response.send({ 'status': 'error', error: 'email and password INVALID' })
            } else {

                response.send(utils.createResult(error, users))
            }
        }
    })

})

router.post('/', (request, response) => {
    const { name, password, mobile, address, email, birth } = request.body
    const statement = `insert into customers (name,password,mobile,address,email,birth) values('${name}','${password}','${mobile}','${address}','${email}','${birth}')`
    db.query(statement, (error, data) => {

        response.send(utils.createResult(error, data))
    })
})


// =======================================================================================================
// put
// =======================================================================================================


router.put('/:id', (request, response) => {
    const { id } = request.params
    const { name, password, mobile, address, email, birth } = request.body
    const statement = `update  customers set name='${name}',password='${password}',mobile='${mobile}',address='${address}',email='${email}',birth='${birth}' where id=${id}`
    db.query(statement, (error, data) => {

        response.send(utils.createResult(error, data))
    })

})

// =======================================================================================================
// delete
// =======================================================================================================


router.delete('/:id', (request, response) => {
    const { id } = request.params

    const statement = `delete from customers where id=${id}`
    db.query(statement, (error, data) => {

        response.send(utils.createResult(error, data))
    })
})

module.exports = router