function createResult(error, data) {
    return error ? createError(error) : createSuccess(data)
}

function createError(error) {
    return { 'status': 'error', error: error }
}

function createSuccess(data) {
    return { 'status': 'success', data: data }
}
module.exports = {
    createResult: createResult,
    createError: createError,
    createSuccess: createSuccess
}