const express = require('express')


//routers
const userRouter = require('./routes/user')
const bookRouter = require('./routes/book')
const bodyParser = require('body-parser')
const db = require('./db')
const app = express()

app.use(bodyParser.json())


//add the routers
app.use('/user', userRouter)
app.use('/book', bookRouter)


app.listen(3000, '0.0.0.0', () => {
    console.log("server lestening on port 3000");
})