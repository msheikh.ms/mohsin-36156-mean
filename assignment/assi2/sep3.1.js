const express = require('express')

const bodyParser = require('body-parser')
const mysql = require('mysql')
const { request, response } = require('express')
const app = express()

app.use(bodyParser.json())


app.get('/pizza_item', (request, response) => {

    // creating a mysql connection
    const connection = mysql.createConnection({
      host: 'localhost',
      user: 'root',
      password: 'password',
      database: 'mydb'
    })
    connection.connect()
    const statement = `select id, name,type, category,description from pizza_item`
    connection.query(statement, (error, data) => {

      connection.end()
  
      if (error) {
        response.end('error while executing query')
        console.log(`${error}`)
      } else {
        response.send(data)
      }
    })
  
  })
  app.post('/pizza_item',(request,response)=>{
    const connection = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: 'password',
        database: 'mydb'
      })
      connection.connect()
    const statement = `insert into pizza_item 
    (name,type,category,description) 
    values ('${request.body.name}', 
            '${request.body.type}',
            '${request.body.category}', 
            '${request.body.description}')`
    connection.query(statement,(error,data)=>{
        console.log(`${error}`)
        connection.end()
        response.send(data)
    })
  })


  app.put('/pizza_item',(request,response)=>{
      const connection = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: 'password',
        database: 'mydb'
      })
      connection.connect()
      const statement = `update pizza_item 
      set name = '${request.body.name}',
      type = '${request.body.type}',
      category = '${request.body.category}',
      description = '${request.body.description}'
      where id = ${request.body.id}`



      connection.query(statement,(error,data)=>{
        console.log(error)  
        connection.end()
        response.send(data)
      })
  })



  app.delete('/pizza_item',(request,response)=>{
    const connection = mysql.createConnection({
      host: 'localhost',
      user: 'root',
      password: 'password',
      database: 'mydb'
    })
    connection.connect()
    const statement = `delete from pizza_item where id = ${request.body.id}`

    connection.query(statement,(error,data)=>{
      console.log(error)  
      connection.end()
      response.send(data)
    })
})

app.listen(3000, '0.0.0.0', () => {
    console.log('server started on port 3000')
  })