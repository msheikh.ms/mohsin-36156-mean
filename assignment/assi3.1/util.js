function createResult(error,dbresult){
    const result={ status:'' }

    if(error){
        result['status']="error";
        result['error']=error;
    }else{
        result['status']="success";
        result['data']=dbresult;
    }
    return result

}
module.exports = {
    createResult:createResult
}