const express = require('express')
const db = require('../db')
const util = require('../util')
const router = express();


router.get('/',(request,responce)=>{

 const statement = `select id, name, password, mobile, address, email from customer`

    db.query(statement,(error,dbresult)=>{
        const result = util.createResult(error,dbresult)
        responce.send(result)
    })
})

router.post('/',(request,responce)=>{
    const name=request.body.name
    const password=request.body.password
    const mobile=request.body.mobile
    const address=request.body.address
    const email=request.body.email


    const statement=`insert into Customer(name,password,mobile,address,email) 
    values
    ('${name}',
    '${password}',
    '${mobile}',
    '${address}',
    '${email}')`

    db.query(statement,(error,dbresult)=>{

        const result=utils.createResult(error,dbresult)
        response.send(result);
    })
})


router.put('/',(request,response)=>{

    const name=request.body.name
    const password=request.body.password
    const mobile=request.body.mobile
    const address=request.body.address
    const email=request.body.email
    const id=request.body.id

    const statement=`update Customer
                     set name= '${name}',
                     password= '${password}',
                     mobile='${mobile}',
                     address='${address}',
                     email='${email}'
                     where id=${id}`                

    db.query(statement,(error,dbresult)=>{

        const result=utils.createResult(error,dbresult)
        response.send(result);
    })
})

router.delete('/',(request,response)=>{

    const id=request.body.id

    const statement=`delete from Customer where id=${id}`

    db.query(statement,(error,dbresult)=>{

        const result=utils.createResult(error,dbresult)
        response.send(result);
    })
})
module.exports=router;