const express = require('express')
const bodyPraser = require('body-parser')

const routeCustomer = require('./route/customer')

const app = express()
app.use(bodyPraser.json())
app.use('/customer',routeCustomer)

app.listen(3000,'0.0.0.0',()=>{
    console.log('server started at port no 3000');
})